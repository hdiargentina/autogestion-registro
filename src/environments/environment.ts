// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //urlService: 'https://192.18.6.9/hdi/sb',
  urlService: 'https://api.hdi.com.ar/hdi/sb',
  clientId: '4f5e4076-c053-41b1-b7d2-4e7b1ee735cc',
  clientSecret: 'yD6eF7fI4pF8jE6uL4vT4uK2mU1oK0hC4kY7lA8nH7yW1mQ5gV',
  scope: 'Polizas',
  grant_type: 'client_credentials'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
