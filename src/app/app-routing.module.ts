
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error500Component } from './pages/500-error/500-error.component';
import { RegistroComponent } from './components/registro/registro.component';
import { LoginLayoutComponent } from './components/login-layout/login-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';

const routes: Routes = [
    { path: '', redirectTo: '/index', pathMatch: 'full' },
    { 
      path: 'index', component: LoginLayoutComponent, children: [
        { path: '', component: RegistroComponent,  },
      ]
    },
    /*{ path: '500-error', component: MainLayoutComponent,
    children: [
      { path: '', component: Error500Component },
    ]},
    { path: '**', redirectTo:'/500-error', pathMatch: 'full'},*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }