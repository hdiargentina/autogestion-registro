import { NgModule } from '@angular/core';
import { HdiIfOnDomDirective } from './hdi-if-on-dom/hdi-if-on-dom.directive';

@NgModule({
    declarations: [
        HdiIfOnDomDirective
    ],
    imports     : [],
    exports     : [
        HdiIfOnDomDirective
    ]
})
export class HdiDirectivesModule{ }