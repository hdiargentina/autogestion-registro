import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Usuario } from '../components/registro/usuario';

@Injectable({
    providedIn: 'root'
  })
export class AseguradoService {

    constructor(
        private _http: HttpClient
    ) { }

    private extractData(res: Response) {
        let body = res;
        return body || { };
      }

    public validarAsegurado(tipoDocumento, numeroDocumento): Observable<HttpResponse<Object>> {
        let data = {
            "empresa": "A",
            "sucursal": "CA",
            "tipoDocumento": tipoDocumento,
            "numeroDocumento": numeroDocumento
        }
        console.log(data);
        return this._http.post<Observable<HttpResponse<Object>>>(environment.urlService +'/autogestion/validarAsegurado', data, {observe: 'response'})
        .pipe(
            tap(resp=>console.log("response", resp))
        )
    }
    public validarNumeroPoliza(usuario: Usuario, numeroPoliza): Observable<any> {
        return this._http.get(environment.urlService + "/autogestion/validarPolizaPorAsegurado?numeroPoliza="+numeroPoliza+"&&numeroDocumento="+usuario.numeroDocumento+"&tipoDocumento="+usuario.tipoDocumento+"").pipe(
            map(this.extractData)
        )
    }
}