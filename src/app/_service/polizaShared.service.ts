import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface Base{
    empr: String,
    sucu: String, 
    nivt: String, 
    nivc: String, 
    nit1: String,
    niv1: String
}

export interface Poliza{
    arse: String,
    articulo: String,
    base: Base,
    certificado: String,
    codigoRama: String,
    nroPoliza: String,
    operacion: String,
    rama: String,
    superpoliza: String,
    tipoPoliza: String,
    tipoRama: String,
    vigenDesde: String,
    vigenHasta: String,
}

@Injectable()
export class PolizaSharedService {
    private poliza: any
    private bien:any
    public tipoDocumento:Observable<any>;
    public numeroDocumento:Observable<any>;
    public logedIn:Boolean;
    
    constructor(
        
    ) {
        this.poliza = {};
        this.bien = {};
     }
    public getPoliza() {
        return this.poliza
    }
    public setPoliza(poliza) {
        this.poliza = poliza;
    }
}