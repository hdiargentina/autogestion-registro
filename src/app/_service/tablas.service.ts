import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { ObservableMedia } from '@angular/flex-layout';

@Injectable()
export class TablasService {

    public tiposTelefonos = [
        {codigo: "P", descripcion: "Particular"},
        {codigo: "C", descripcion: "Celular"},
        {codigo: "E", descripcion: "Empresa"}
    ];
      
    constructor(
        private _http: HttpClient
    ) { }

    private extractData(res: Response) {
        let body = res;
        return body || { };
      }

    public getFormasPago(): Observable<any> {
        return this._http.get(environment.urlService +'/apitablas/getTablaListaFormasDePago').pipe(map(this.extractData))
    }
    public getListaEmpresaTC(): Observable<any> { 
        return this._http.get(environment.urlService +'/apitablas/getTablaListaTarjetasDeCredito').pipe(map(this.extractData))
    }
    public getListaTipoMail(): Observable<any> { 
        return this._http.get(environment.urlService +'/apitablas/getTablaListaTiposDeMail').pipe(map(this.extractData))
    }
    public getListaNacionalidades(): Observable<any> {
        return this._http.get(environment.urlService +'/apitablas/getTablaListaNacionalidades').pipe(map(this.extractData))
    }
    public getListaEstadoCivil(): Observable<any> {
        return this._http.get(environment.urlService + "/apitablas/getTablaListaEstadoCivil").pipe(map(this.extractData))
    }
    public getListaTiposTelefono(): any {
        return this.tiposTelefonos;
    }
    public getListaRamaActividadEconomica(): Observable<any> {
        return this._http.get(environment.urlService +'/apitablas/getTablaListaRamaEconomica').pipe(map(this.extractData))
    }
    public getListaProfesiones(): Observable<any> {
        return this._http.get(environment.urlService +'/apitablas/getTablaListaProfesiones').pipe(map(this.extractData))
    }
    public getTablas(filtro, nombre): any {
        let data = {
            "nombre": nombre,
            "filtro": filtro
        }
        console.log(data)
        return this._http.post(environment.urlService+"/quomrest/getTabla", data).pipe(
            map(this.extractData)
        );
    }
}