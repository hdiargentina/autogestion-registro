import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { TokenService } from './token.service';
import {throwError as observableThrowError,  Observable  } from 'rxjs';
import { environment } from '../../environments/environment';
import { catchError, switchMap } from 'rxjs/operators';
import { 
    HttpInterceptor, 
    HttpRequest, 
    HttpHandler, 
    HttpSentEvent, 
    HttpHeaderResponse, 
    HttpProgressEvent, 
    HttpResponse, 
    HttpUserEvent, 
    HttpErrorResponse 
} from "@angular/common/http";
import { error } from '@angular/compiler/src/util';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private _router: Router,
        private _tokenService: TokenService
    ) { }
    
    private _addHeaders(req: HttpRequest<any>): HttpRequest<any> {
        if (req.url.includes("token")) {
            return req;
        }
        
        return req.clone({
            setHeaders: {
                'X-IBM-Client-Id': environment.clientId,
                scope: environment.scope,
                Authorization: `Bearer ${this._tokenService.getToken()}`,
                'Content-Type': 'application/json'
            }
        });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        return next.handle(this._addHeaders(request)).pipe(
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    switch((<HttpErrorResponse>err).status){
                        case 400:
                            return this.handle400Error(err);
                        case 401:
                            return this.handle401Error(request, next);
                        case 500:
                            return this.handle500Error(error);
                        default:
                            return this.handle500Error(error);
                    }
                } else {
                    return observableThrowError(err);
                }
            })
        )
    }

    handle400Error(error) {
        return observableThrowError(error);
    }

    handle401Error(req: HttpRequest<any>, next: HttpHandler) {
        return this._tokenService.generateToken().pipe(
            switchMap(resp => {
                if (resp) {
                    return next.handle(this._addHeaders(req));
                }
            }),
            catchError(error => {
                return observableThrowError(error);
            })
        )
    }

    handle500Error(error){
        this._router.navigate(['/500-error']);
        return observableThrowError(error);
    }
}