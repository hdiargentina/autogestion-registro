import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable()
export class TokenService {
    public cachedRequests: Array<HttpRequest<any>> = [];

    constructor(
        private _http: HttpClient
    ) { }

    public generateToken() {
        let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
        let params = new HttpParams()
            .set('client_id', environment.clientId)
            .set('client_secret',environment.clientSecret)
            .set('scope',environment.scope)
            .set('grant_type',environment.grant_type);
        return this._http.post(environment.urlService + '/proveedordeoauthhdi/oauth2/token',params,{ headers: headers })
            .pipe(map ((resp:any) => {
                localStorage.setItem('token', resp.access_token );
                return true;
            })
        )
    }

    public collectFailedRequest(request): void {
        this.cachedRequests.push(request);
    }

    public getToken(): string {
        return localStorage.getItem('token');
    }

    
}