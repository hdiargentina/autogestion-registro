import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClientModule } from '@angular/common/http';

import { TokenService } from './_service/token.service';
import { LoaderService } from './_service/loader.service';

import { SharedModule } from './shared/shared.module';
import { ComponentsModule } from './components/components.module';
import { PerfilModule } from './perfil/perfil.module';
import { PagesModule } from './pages/pages.module';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './_service/token.interceptor';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MainLayoutComponent } from './main-layout/main-layout.component';




@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    ComponentsModule,
    PerfilModule,
    PagesModule,
  ],
  providers: [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true
    },
    TokenService,
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
