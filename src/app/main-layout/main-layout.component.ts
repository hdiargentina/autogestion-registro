import { Component, OnInit } from '@angular/core';
import { 
    Router
} from '@angular/router';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { LoaderService } from '../_service/loader.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['../app.component.css']
})
export class MainLayoutComponent implements OnInit {
  public opened = true;
  public over = 'side';
  public expandHeight = '42px';
  public collapseHeight = '42px';
  public displayMode = 'flat';
  public loading: Boolean;

  watcher: Subscription;

  constructor(
      private _media: ObservableMedia,
      private _loaderService: LoaderService,
      private _router: Router
  ) {
      this.watcher = this._media.subscribe((change: MediaChange) => {
          if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {
              this.opened = false;
              this.over = 'over';
          } else {
              this.opened = true;
              this.over = 'side';
          }
      });
  }

  ngOnInit() {
      this._loaderService.status.subscribe((val: boolean) => {
          this.loading = val;
      });
  }

}
