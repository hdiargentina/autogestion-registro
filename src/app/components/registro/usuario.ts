export class Usuario {
    public password: string;    
    public cn: string;
    public sn: string;
    public mail: string;
    public tipoDocumento: string;
    public numeroDocumento: string;
}