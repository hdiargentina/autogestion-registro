import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioSharedService } from '../usuario-shared.service';

@Component({
  selector: 'datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.css']
})
export class DatosPersonalesComponent implements OnInit {
  public tipoDocumento:any;
  public maxLengthNombre=20;
  public maxLengthApellido=30;
  public maxLengthDocumento=0;
  public formGroup: FormGroup;
  public minNombre=3;
  public minApellido=3;
  public minDoc=3;
  allowedChars = new Set('0123456789'.split('').map(c => c.charCodeAt(0)));
  public tiposDocumento = [
    {"codigo":"4", "descripcion": "DNI"},
    {"codigo": "98", "descripcion": "CUIT"},
    {"codigo": "2", "descripcion": "LC"},
    {"codigo": "1", "descripcion": "LE"}
  ];
  
  
  constructor(
    private formBuilder: FormBuilder,
    private _usuarioShared: UsuarioSharedService
    ) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      nombre: ['', [
        Validators.required, 
        Validators.minLength(this.minNombre)]
      ],
      apellido: ['', [
        Validators.required, 
        Validators.minLength(this.minApellido)]
      ],
      numeroDocumento: ['', [
        Validators.required, 
        //Validators.min(this.minDoc),
        //Validators.max(this.maxLengthDocumento)
      ]
      ]
    });
  }
  public setLengthDocumento(tipoDocumento) {
    if(tipoDocumento == '4') {
      this.maxLengthDocumento = 8;
    } else {
      this.maxLengthDocumento = 11;
    }
  }
  public tipoDocumentoChange(event) {
    let value = event.value.toLowerCase();
    console.log("valor: ",value);
    this.setLengthDocumento(value)
    this._usuarioShared.getUsuario().tipoDocumento = value;
    console.log(this._usuarioShared.getUsuario())
  }
  get nombre() { return this.formGroup.get('nombre'); }  
  get apellido() { return this.formGroup.get('apellido'); }  
  get numeroDocumento() { return this.formGroup.get('numeroDocumento'); }  
  
  check(event: KeyboardEvent) {
    // 31 and below are control keys, don't block them.
    if (event.keyCode > 31 && !this.allowedChars.has(event.keyCode)) {
      event.preventDefault();
    }
  }
  onNombreChange(args) {
    console.log(this._usuarioShared.getUsuario())
    this._usuarioShared.getUsuario().cn = args;
  }
  onApellidoChange(args) {
    console.log(this._usuarioShared.getUsuario())
    this._usuarioShared.getUsuario().sn = args;
  }
  onDocumentoChange(args) {
    console.log(this._usuarioShared.getUsuario())
    this._usuarioShared.getUsuario().numeroDocumento = args;
  }
}
