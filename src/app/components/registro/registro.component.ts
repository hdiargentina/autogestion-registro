import { Component, OnInit, Inject } from '@angular/core';
import { Usuario } from './usuario';
import { MatDialog } from '@angular/material';
import { PopupPolizaComponent } from '../popup-poliza/popup-poliza.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { UsuarioSharedService } from './usuario-shared.service';
import { RegistroService } from './registro.service';
import { ModalProcesoOkComponent } from '../modal-proceso-ok/modal-proceso-ok.component';
import { DOCUMENT } from '@angular/common';
import { Validators, FormBuilder, FormGroup, ValidatorFn, ValidationErrors } from '@angular/forms';
import { AseguradoService } from '../../_service/asegurado.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  public usuario: Usuario;
  public poliza: string;
  public formGroup: FormGroup;
  private maxLengthDocumento = 4;
  constructor(
    private _dialog: MatDialog,
    private _usuarioShared: UsuarioSharedService,
    private _registroService: RegistroService,
    private formBuilder: FormBuilder,
    private _aseguradoService: AseguradoService,
    @Inject(DOCUMENT) private document: any
  ) { 
    this.usuario = new Usuario();
    
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      nombre: ['', [
        Validators.required, 
        Validators.minLength(3)]
      ],
      apellido: ['', [
        Validators.required, 
        Validators.minLength(3)]
      ],
      numeroDocumento: ['', [
        Validators.required, 
        /*Validators.min(3),
        Validators.max(16)*/
      ]],
      tipoDocumento: ['', [
        Validators.required, 
        /*Validators.min(3),
        Validators.max(16)*/
      ]],
      email: ['',[
        Validators.required,
      ]],
      password: ['',[
        Validators.required,
      ]],
      password2: ['',[
        Validators.required,
      ]],
      poliza: ['',[
        Validators.required,
      ]],
    }, {validator: passwordMatchValidator});
  }
  onPasswordInput() {
    if (this.formGroup.hasError('passwordMismatch')){
      this.password2.setErrors([{'passwordMismatch': true}]);
      this._usuarioShared.getUsuario().password
    }
    else{
      this.password2.setErrors(null);
      this._usuarioShared.getUsuario().password = this.password.value;
    }
    console.log(this._usuarioShared.getUsuario())
  }

  get tipoDocumento() {return this.formGroup.get('tipoDocumento')}

  onClick() {
    let usuario = new Usuario();
    usuario.tipoDocumento = this.formGroup.get('tipoDocumento').value;
    usuario.numeroDocumento = this.formGroup.get('numeroDocumento').value;
    usuario.mail = this.formGroup.get('email').value;
    usuario.cn = this.formGroup.get('nombre').value;
    usuario.sn = this.formGroup.get('apellido').value;
    usuario.password = this.formGroup.get("password").value;
    if(this.formGroup.valid) {
      console.log("en el true", usuario);
      this._aseguradoService.validarNumeroPoliza(usuario, this.formGroup.get("poliza").value).subscribe(data=>{
        if(data.mensaje) {
          this._registroService.guardarUsuario(usuario).subscribe(data=> {
              if(data.httpCode ==	200 && data.httpMessage != "No se pudo crear el usuario") { 
              this.abrirPopupProcesoOk(data.httpMessage, data.moreInformation);
            } else {
              this.abrirPopupProcesoOk(data.httpMessage, data.moreInformation);
            } 
            //alert(data.moreInformation)
            
          });
        } else {
          alert("El numero de poliza ingresado no existe en nuestras bases. Por favor, vuelva a intentarlo");
        }
      })
    } else  {
      console.log("en el false", this.isEmpty(this._usuarioShared.getUsuario()))
      this.abrirPopupProcesoOk("Debe cargar todos los campos", "Por favor, revise los campos que se encuentran de color rojo");
    }
  }
  isEmpty(obj) {
    for(var prop in obj) {
       if (obj.hasOwnProperty(prop)) {
          return false;
       }
    }
    return true;
  }
  abrirImagen() {
        const dialogRef = this._dialog.open(PopupPolizaComponent, {
            data: {}
        });
        dialogRef.afterClosed().subscribe(result => {
        });
    }
  abrirPopupProcesoOk(titulo: string, body: string) {
      const dialogRef = this._dialog.open(ModalProcesoOkComponent, {
          data: {
            titulo: titulo, 
            body: body
          }
      });
      
      dialogRef.afterClosed().subscribe(result => {
       // this.document.location.href = 'https://isamtest.hdi.com.ar/pkmslogout';
      });
  }
 
  public setLengthDocumento(tipoDocumento) {
    if(tipoDocumento == '4') {
      this.maxLengthDocumento = 8;
    } else {
      this.maxLengthDocumento = 11;
    }
  }
  tiposDocumento = [
    {"codigo":"4", "descripcion": "DNI"},
    {"codigo": "98", "descripcion": "CUIT"},
    {"codigo": "2", "descripcion": "LC"},
    {"codigo": "1", "descripcion": "LE"}
  ];
  get password() { return this.formGroup.get('password'); }
  get password2() { return this.formGroup.get('password2'); }
}
export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  if (formGroup.get('password').value === formGroup.get('password2').value)
    return null;
  else
    return {passwordMismatch: true};
};