import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioSharedService {
  private usuario: Usuario;
  constructor(
  ) { 
    this.usuario =new Usuario();
  }
  getUsuario() {
    return this.usuario;
  }
  setUsuario(usuario: Usuario) {
    this.usuario = usuario;
  }
}