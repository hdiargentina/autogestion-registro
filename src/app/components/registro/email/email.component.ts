import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UsuarioSharedService } from '../usuario-shared.service';

@Component({
  selector: 'email-component',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  public email:string;
  public maxLengthMail=50;
  public emailFormControl: FormGroup;
  public formGroup: FormGroup;
 
  constructor(
    private _usuarioShared: UsuarioSharedService
  ) {
    
   }

  ngOnInit() 
  {
  
  }
  onMailChange(args) {
    this._usuarioShared.getUsuario().mail = args;
  }
}
