import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { UsuarioSharedService } from '../usuario-shared.service';

@Component({
  selector: 'password-component',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  public maxLengthPassword=15;
  public mostrarPass=false;
  public minPw=8;

  formGroup: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private _usuarioShared: UsuarioSharedService
    ) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(this.minPw)]],
      password2: ['', [Validators.required]]
    }, {validator: passwordMatchValidator});
  }
  onPasswordInput() {
    if (this.formGroup.hasError('passwordMismatch')){
      this.password2.setErrors([{'passwordMismatch': true}]);
      this._usuarioShared.getUsuario().password
    }
    else{
      this.password2.setErrors(null);
      this._usuarioShared.getUsuario().password = this.password.value;
    }
    console.log(this._usuarioShared.getUsuario())
  }

  get password() { return this.formGroup.get('password'); }
  get password2() { return this.formGroup.get('password2'); }

}

export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  if (formGroup.get('password').value === formGroup.get('password2').value)
    return null;
  else
    return {passwordMismatch: true};
};