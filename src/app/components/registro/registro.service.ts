import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(
    private _http: HttpClient
  ) { }
  
  public guardarUsuario(usuario: Usuario): Observable<any> {
    let body = {
      "usuario": usuario.tipoDocumento+usuario.numeroDocumento,
      "password": usuario.password,
      "nombre": usuario.cn,
      "apellido": usuario.sn,
      "mail": usuario.mail,
      "cuit": usuario.numeroDocumento,
      "tipoDocumento": usuario.tipoDocumento,
      "numeroDocumento":usuario.numeroDocumento
    };
    console.log("body", body)
    return this._http.post(environment.urlService + '/autogestion/usuarios/usuario',body);
  }
  public obtenerNumerosPoliza(usuario:Usuario):Observable<any>  {//
    return this._http.get(environment.urlService + '/autogestion/getPolizasPorAsegurado?empresa=A&sucursal=CA&tipoDocumento='+usuario.tipoDocumento+'+&numeroDocumento='+usuario.numeroDocumento);
  }
} 
