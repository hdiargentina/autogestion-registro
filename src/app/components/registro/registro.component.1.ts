/*import { Component, OnInit, Inject } from '@angular/core';
import { Usuario } from './usuario';
import { MatDialog } from '@angular/material';
import { PopupPolizaComponent } from '../popup-poliza/popup-poliza.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { UsuarioSharedService } from './usuario-shared.service';
import { RegistroService } from './registro.service';
import { ModalProcesoOkComponent } from '../modal-proceso-ok/modal-proceso-ok.component';
import { DOCUMENT } from '@angular/common';
import { PasswordComponent } from './password/password.component';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  public usuario: Usuario;
  public poliza: string;
  constructor(
    private _dialog: MatDialog,
    private _usuarioShared: UsuarioSharedService,
    private _registroService: RegistroService,
    @Inject(DOCUMENT) private document: any
  ) { 
    this.usuario = new Usuario();
  }

  ngOnInit() {
    console.log(document);
  }
  onClick() {
    let polizaExiste = false;
    console.log("usuario shared", this._usuarioShared.getUsuario())
    if(!this.isEmpty(this._usuarioShared.getUsuario())) {
      console.log("en el true", this.isEmpty(this._usuarioShared.getUsuario()));
      this._registroService.obtenerNumerosPoliza(this._usuarioShared.getUsuario()).subscribe(data=>{
        let polizas = data.polizas.poliza;
        for(let poliza of polizas) {
          if(poliza.nroPoliza == this.poliza) {
           polizaExiste = true;
          } 
        }
        if(polizaExiste) {
          this._registroService.guardarUsuario(this._usuarioShared.getUsuario()).subscribe(data=> {
              if(data.httpCode ==	200 && data.httpMessage != "No se pudo crear el usuario") { 
              this.abrirPopupProcesoOk(data.httpMessage, data.moreInformation);
            } else {
              this.abrirPopupProcesoOk(data.httpMessage, data.moreInformation);
            } 
            //alert(data.moreInformation)
            
          });
        } else {
          alert("El numero de poliza ingresado no existe en nuestras bases. Por favor, vuelva a intentarlo");
        }
      })
    } else  {
      console.log("en el false", this.isEmpty(this._usuarioShared.getUsuario()))
      this.abrirPopupProcesoOk("Debe cargar todos los campos", "Por favor, revise los campos que se encuentran de color rojo");
    }
    
  }
  isEmpty(obj) {
    for(var prop in obj) {
       if (obj.hasOwnProperty(prop)) {
          return false;
       }
    }

    return true;
  }
  abrirImagen() {
        const dialogRef = this._dialog.open(PopupPolizaComponent, {
            data: {}
        });
        
        dialogRef.afterClosed().subscribe(result => {
            
        });
    }
  abrirPopupProcesoOk(titulo: string, body: string) {
      const dialogRef = this._dialog.open(ModalProcesoOkComponent, {
          data: {
            titulo: titulo, 
            body: body
          }
      });
      
      dialogRef.afterClosed().subscribe(result => {
        this.document.location.href = 'https://isamtest.hdi.com.ar/pkmslogout';
      });
  }
}
*/