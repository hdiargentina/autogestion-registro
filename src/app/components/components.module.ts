import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module'

import { SidenavComponent } from './sidenav/sidenav.component';
import { TopbarComponent } from './topbar/topbar.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { LoginComponent } from './login/login.component';
import { PolizasSubmenuComponent } from './sidenav/polizas-submenu/polizas-submenu.component';
import { RegistroComponent } from './registro/registro.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { PasswordComponent } from './registro/password/password.component';
import { EmailComponent } from './registro/email/email.component';
import { DatosPersonalesComponent } from './registro/datos-personales/datos-personales.component';
import { PopupPolizaComponent } from './popup-poliza/popup-poliza.component';
import { ModalProcesoOkComponent } from './modal-proceso-ok/modal-proceso-ok.component';


@NgModule({
    declarations: [
        SidenavComponent,
        TopbarComponent,
        BreadcrumbComponent,
        LoginComponent,
        PolizasSubmenuComponent,
        RegistroComponent,
        LoginLayoutComponent,
        PasswordComponent,
        EmailComponent,
        DatosPersonalesComponent,
        PopupPolizaComponent,
        ModalProcesoOkComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        SharedModule
    ],
    entryComponents:[PopupPolizaComponent, ModalProcesoOkComponent],
    exports: [
        SidenavComponent,
        TopbarComponent,
        BreadcrumbComponent,
        LoginComponent,
        PopupPolizaComponent
    ]
})
export class ComponentsModule{ }