import { AseguradoService } from './../../_service/asegurado.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { PolizaSharedService } from '../../_service/polizaShared.service';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    @Input() logedIn;
    @Output() logedinChange=new EventEmitter();
    public tipoDocumento:any;
    public numeroDocumento:any;
    public headers: any;
    constructor(
        private _router: Router, 
        private _polizaSharedService:PolizaSharedService,
        private _aseguradoService: AseguradoService
    ) {

    }

    ngOnInit() { 
        console.log("headers", this.headers);
    }
    public tiposDocumento= [
        {"codigo":"4", "descripcion": "DNI"},
        {"codigo": "98", "descripcion": "CUIT"}        
    ];
    public onClick() {
        console.log(this.logedIn)
        this.logedinChange.emit(true);
        this._aseguradoService.validarAsegurado(this.tipoDocumento, this.numeroDocumento).subscribe(data=>{
            console.log(data)
            if(data.statusText ==='OK') {
                this.logedIn = true;
                this._polizaSharedService.numeroDocumento = this.numeroDocumento;
                this._polizaSharedService.tipoDocumento = this.tipoDocumento;
                this._polizaSharedService.logedIn = true;
                this._router.navigate(['/dashboard'])
            } else {
                alert(data.statusText)
            }
        })
    }
    setHeaders(args) {
        console.log(args);
    } 
}