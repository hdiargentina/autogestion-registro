import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegistroComponent } from '../registro/registro.component';

@Component({
  selector: 'app-modal-proceso-ok',
  templateUrl: './modal-proceso-ok.component.html',
  styleUrls: ['./modal-proceso-ok.component.css']
})
export class ModalProcesoOkComponent implements OnInit {
  public titulo: string;
  public body: string;
  constructor(
    public dialogRef: MatDialogRef<RegistroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.titulo = this.data.titulo;
    this.body = this.data.body;
  }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close();
  }
}
