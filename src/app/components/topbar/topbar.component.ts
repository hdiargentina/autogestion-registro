import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'hdi-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

    @Input() sidenavComponent;

    ngOnInit() { }

}