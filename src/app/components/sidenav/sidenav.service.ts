import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class SidenavService {
    constructor(
        private _http: HttpClient
    ) { }

    public getRamas(tipoDocumento, numeroDocumento): Observable<any> {
        let data = {
            "empresa":"A",
	        "sucursal":"CA",
	        "tipoDocumento":tipoDocumento,
	        "numeroDocumento":numeroDocumento
        }
        return this._http.post(environment.urlService + '/autogestion/getRamasAsegurado',data);
    }
    public getPolizas(tipoDocumento, numeroDocumento): Observable<any> {
        let data = {
            "empresa":"A",
	        "sucursal":"CA",
	        "tipoDocumento":tipoDocumento,
	        "numeroDocumento":numeroDocumento
        }
        return this._http.post(environment.urlService + '/autogestion/getPolizasPorAsegurado',data)
    }
}