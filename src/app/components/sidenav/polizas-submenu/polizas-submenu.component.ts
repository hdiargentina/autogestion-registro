import { Component, OnInit, Input } from '@angular/core';
import { PolizaSharedService } from '../../../_service/polizaShared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'polizas-submenu',
  templateUrl: './polizas-submenu.component.html',
  styleUrls: ['./polizas-submenu.component.css']
})
export class PolizasSubmenuComponent implements OnInit {
  @Input() lista;
  @Input() titulo;
  @Input() icono;
  constructor(
    private _router:Router,
    private _polizaSharedService: PolizaSharedService
  ) 
  {
    
  }

  ngOnInit() {
    console.log("Lista: ", this.lista);
    this.titulo = this.lista[0].rama;
    console.log(this.titulo)
  }
  public seleccionarPoliza(poliza) {
    this._polizaSharedService.setPoliza(poliza);
    this._router.navigate([`/poliza/`]);
  }
}
