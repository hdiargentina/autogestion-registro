import { Component, OnInit } from '@angular/core';
import { ObservableMedia, MediaChange, DEFAULT_BREAKPOINTS } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { SidenavService } from './sidenav.service';
import { PolizaSharedService } from '../../_service/polizaShared.service';
import { Router } from '@angular/router';


@Component({
  selector: 'hdi-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
  providers: [ SidenavService ]
})
export class SidenavComponent implements OnInit {
    public opened = true;
    public over = 'side';
    public expandHeight = '42px';
    public collapseHeight = '42px';
    public displayMode = 'flat';
    
    public autos;
    public hogar;
    public ap;
    public transporteCasco;
    public integralConsorcio;
    public riesgosVarios;
    public mercaderias;
    public integralComercio;
    public vida;
    private ramas;
    private logedin;
    private polizas: any;
    public polizasAutos:any;
    public polizasHogar:any;
    public polizasTc:any;
    public polizasMercaderias:any;
    public polizasIntegralComercio;any;
    public polizasRv:any;
    public polizasVida:any;
    public polizasConsorcio:any;
    public polizasAp: any;
    public  :any;
  // overlap = false;
    watcher: Subscription;

    constructor(
        private _media: ObservableMedia,
        private _sidenavService:SidenavService,
        private _router:Router,
        private _polizaSharedService: PolizaSharedService
        ) {
        this.watcher = _media.subscribe((change: MediaChange) => {
            if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {
                this.opened = false;
                this.over = 'over';
            } else {
                this.opened = true;
                this.over = 'side';
            }
        });
    }

    ngOnInit() {
        this.autos = false;
        this.hogar = false;
        this.ap = false;
        this.transporteCasco = false;
        this.integralConsorcio = false;
        this.riesgosVarios = false;
        this.mercaderias = false;
        this.integralComercio = false;
        this.vida = false;
        
     //   this._muestraRamas(); 
    }

    isLogedIn():Boolean {
        this.logedin = false;
        this.logedin=this._polizaSharedService.logedIn;
        if(this.logedin) {
            if(this.polizas) {
                this._obtenerPolizas();
                return this.logedin;
            }
        }
        return this.logedin;
    }

    private _obtenerPolizas() {
        this._sidenavService.getPolizas(this._polizaSharedService.tipoDocumento, this._polizaSharedService.numeroDocumento).subscribe(data => {
            this.polizas = data.polizas.poliza;
            this.polizasAutos = this.polizas.filter(poliza=>poliza.tipoRama==='A');
            this.polizasHogar = this.polizas.filter(poliza=>poliza.tipoRama==='H');
            this.polizasTc = this.polizas.filter(poliza=>poliza.tipoRama==='T');
            this.polizasMercaderias = this.polizas.filter(poliza=>poliza.tipoRama==='M');
            this.polizasIntegralComercio = this.polizas.filter(poliza=>poliza.tipoRama==='R');
            this.polizasRv = this.polizas.filter(poliza=>poliza.tipoRama==='O');
            this.polizasVida = this.polizas.filter(poliza=>poliza.tipoRama==='V');
            this.polizasConsorcio = this.polizas.filter(poliza=>poliza.tipoRama==='C');
            this.polizasAp = this.polizas.filter(poliza=>poliza.tipoRama==='P');
            this._muestraRamas();
            console.log("polizasVida",this.polizasVida);
            console.log("Polizas", this.polizas)
        })
    }
    private _muestraRamas() {
        for(let poliza of this.polizas) {
            switch(poliza.tipoRama){
                case "A":
                    this.autos = true;
                    break;
                case "H":
                    this.hogar = true;
                    break;
                case "T":
                    this.transporteCasco = true;
                    break;
                case "M":
                    this.mercaderias = true;
                    break;
                case "R":
                    this.integralComercio = true;
                    break;
                case "O":
                    this.riesgosVarios = true;
                    break;
                case "V":
                    this.vida = true;
                    break;
                case "C":
                    this.integralConsorcio = true;
                    break;
                case "P":
                    this.ap = true;
                    break;
                default:
                    break;
            }
        }
        
    }
    public seleccionarPoliza(poliza) {
        this._polizaSharedService.setPoliza(poliza);
        this._router.navigate([`/poliza/`]);
    }
}