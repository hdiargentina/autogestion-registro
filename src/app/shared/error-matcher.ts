import { ErrorStateMatcher } from '@angular/material';
import { NgForm, FormGroupDirective, FormControl } from '@angular/forms';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    public isError: boolean;
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      this.isError = !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
      return this.isError;
    }
  }