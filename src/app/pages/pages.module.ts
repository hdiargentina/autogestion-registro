import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { SharedModule } from '../shared/shared.module'
import { Error500Component } from './500-error/500-error.component';

@NgModule({
    declarations: [
        Error500Component,  
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        Error500Component
    ]
})
export class PagesModule{ }