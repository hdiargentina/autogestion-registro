import { MatAutocompleteSelectedEvent } from '@angular/material';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { NacionalidadesService } from './nacionalidades.service';
import { TablasService } from '../../../_service/tablas.service';

@Component({
  selector: 'nacionalidades',
  templateUrl: './nacionalidades.component.html',
  styleUrls: ['./nacionalidades.component.css']
})
export class NacionalidadesComponent implements OnInit {
  public nacionalidades: any;
  public nacionalidadesControl = new FormControl();
  public nacionalidadSeleccionada:any;
  public filteredOptions: Observable<any[]>;
  @Input() asegurado;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService,
  ) {
    this.nacionalidadSeleccionada={}
   }

  ngOnInit() {
    this._tablasService.getListaNacionalidades().subscribe(data=>{
      this.nacionalidades=data.return.Nacionalidades;
      this.nacionalidadSeleccionada = data.return.Nacionalidades.filter(nacionalidad => nacionalidad.CoNomNacionion == this.asegurado.nacionalidad)[0];
      this.asegurado.nacionalidad =this.nacionalidadSeleccionada.CoNomNacionion;
      this.filteredOptions = this.nacionalidadesControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    });
  }
  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();

    return this.nacionalidades.filter(nacionalidad => nacionalidad.NomNacion.toLowerCase().indexOf(filterValue) === 0);
  } 
  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    let value = event.option.value.toLowerCase();
    console.log(value);
    this.nacionalidadSeleccionada = this.nacionalidades.filter(nacionalidad => nacionalidad.NomNacion.toLowerCase().indexOf(value) === 0)[0];
    console.log("nacionalidad seleccionada", this.nacionalidadSeleccionada)
    this.asegurado.nacionalidad = this.nacionalidadSeleccionada.CoNomNacionion;
    this.valueChange.emit(this.asegurado);
    console.log("Nacionalidad Selecc: ",this.asegurado.nacionalidad);
  }
}
