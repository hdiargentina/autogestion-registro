import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TablasService } from '../../../_service/tablas.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material';

@Component({
  selector: 'ramas-actividad-economica',
  templateUrl: './ramas-actividad-economica.component.html',
  styleUrls: ['./ramas-actividad-economica.component.css'],
  providers: [TablasService]
})
export class RamasActividadEconomicaComponent implements OnInit {
  public listaRamaActividadEconomica: any;
  public ramaActividadEconomica: any;
  public ramasControl = new FormControl();
  public filteredOptions: Observable<any[]>;
  public options: string[];
  @Input() asegurado;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService
  ) { 
    this.listaRamaActividadEconomica = {};
    this.ramaActividadEconomica = {};
  }

  ngOnInit() {
    this._tablasService.getListaRamaActividadEconomica() .subscribe(data=>{
      this.listaRamaActividadEconomica = data.return.RamaActEco;
      this.filteredOptions = this.ramasControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
      this.ramaActividadEconomica = this.listaRamaActividadEconomica.filter(ramaAct => ramaAct.CodRamaAct == this.asegurado.ramaActividadEc)[0];
      if(this.ramaActividadEconomica == undefined) 
        this.ramaActividadEconomica = {};
      this.asegurado.ramaActvididadEc = this.ramaActividadEconomica.CodRamaAct;
      console.log(this.asegurado);
      this.valueChange.emit(this.asegurado);
      console.log(this.listaRamaActividadEconomica);
    })
  }
  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();
    return this.listaRamaActividadEconomica.filter(ramaActividad => ramaActividad.NomRamaAct.toLowerCase().indexOf(filterValue) === 0);
  }
  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    let value = event.option.value.toLowerCase();
    console.log(value);
    this.ramaActividadEconomica = this.listaRamaActividadEconomica.filter(ramaActividad => ramaActividad.NomRamaAct.toLowerCase().indexOf(value) === 0)[0];
    console.log(this.ramaActividadEconomica);
    this.asegurado.ramaActividadEc = this.ramaActividadEconomica.CodRamaAct;
    this.valueChange.emit(this.asegurado);
    console.log("ramaAct selec: ",this.asegurado.ramaActividadEc);
  }
}
