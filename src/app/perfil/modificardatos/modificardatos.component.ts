import { Component, OnInit } from '@angular/core';
import { PolizaSharedService } from '../../_service/polizaShared.service';
import { ModificarDatosSservice } from './modificar-datos.service';
import { DatePipe } from '@angular/common';
import { MatSelectChange, MatSnackBar } from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { MyErrorStateMatcher } from '../../shared/error-matcher';
import { TablasService } from '../../_service/tablas.service';

@Component({
  selector: 'app-modificarDatos',
  templateUrl: './modificardatos.component.html',
  styleUrls: ['./modificardatos.component.css'],
  providers: [TablasService, ModificarDatosSservice, DatePipe]
})
export class ModificarDatosComponent implements OnInit {
  public asegurado: any;
  public listaTipoMail:any;
  public matcher = new MyErrorStateMatcher();
  public emailFormControl: any;
  public mensaje: string;
  public show: boolean;
constructor (
  private _modificarDatosService : ModificarDatosSservice,
  private _polizaSharedService: PolizaSharedService,
  private _datePipe: DatePipe,
  private _snackBar: MatSnackBar,
  private _tablasService: TablasService
  ) {
    this.asegurado={};
    this.asegurado.telefonos={};
    this.asegurado.emails = {};
    this.asegurado.emails.email = [];
    this.emailFormControl =  new FormControl('', [
      Validators.required,
      Validators.email,
    ]);
    this.show = true;
  }

  ngOnInit() { 
    this._modificarDatosService.obtenerDatosAsegurado(this._polizaSharedService.tipoDocumento, this._polizaSharedService.numeroDocumento).subscribe(data=>{
      this.show = true;
      this.asegurado = data.asegurado;
      let fechaAux = data.asegurado.fechaNacimiento+"T00:00:00";
      this.asegurado.fechaNacimiento = fechaAux;
      this.asegurado.fechaNacimiento = new Date(fechaAux);
      this.asegurado.fumador = this.asegurado.fumador == '0'?false:true;
      console.log("Mails asegurado: ", this.asegurado.emails.email);
      console.log(this.asegurado);
      this.show = false;
    })
    this._tablasService.getListaTipoMail().subscribe(data=>{
      this.listaTipoMail = data.return.TiposDeMails;

      console.log("Lista tipo mail: ",this.listaTipoMail);
    });
  }
  getRama(){
    console.log(this.asegurado.ramaActvididadEc);
    return this.asegurado.ramaActvididadEc;
  }

  private callSnackBar(textoSnackbar):void {
    this._snackBar.open(textoSnackbar, 'Info', {
      duration: 4500,
    });
  }

  actualizarDatos() {
    console.log("Error matcher ",this.matcher.isError)
    if(this.matcher.isError) {
      this.callSnackBar("Revise los campos ingresados")
    }else {
      this.asegurado.fechaNacimiento = this._datePipe.transform(this.asegurado.fechaNacimiento, 'yyyy-MM-dd');
      this._modificarDatosService.actualizarDatos(this.asegurado, this._polizaSharedService.tipoDocumento, this._polizaSharedService.numeroDocumento).subscribe(data=> {
        let resultado = data.resultado;
        console.log(resultado)
        if(resultado == "OK") {
          this.callSnackBar("Datos actualizados correctamente!");
        }
      });
    }
  }
  actualizarDatosFiliatorios() {
    this._modificarDatosService.actualizarDatosFiliatorios(this._polizaSharedService.tipoDocumento, this._polizaSharedService.numeroDocumento, this.mensaje).subscribe(data=>{
      let result = data.result;
      if(result == "OK") {
        this.callSnackBar("Datos enviados correctamente!");
      }
    })
  } 
  agregarTelefono() {
    let telefono = {
      "tipo": "1",
      "numero": ""
    }
    this.asegurado.telefonos.telefono.push(telefono);
  }
  agregarMail() {
    let mail = {
      "tipo": "",
      "mail": ""
    };
    this.asegurado.emails.email.push(mail);
    console.log("mails after add ", this.asegurado.emails.email)
  }
  borrarTelefono(telefonoSeleccionado) {
    this.asegurado.telefonos.telefono = this.asegurado.telefonos.telefono.filter(telefono => telefono !== telefonoSeleccionado);
  }
  borrarMail(mailSeleccionado) {
    this.asegurado.emails.email = this.asegurado.emails.email.filter(mail => mail !==  mailSeleccionado);
    console.log("mails after delete ", this.asegurado.emails.email)
  }
  onSelectionChanged(event: MatSelectChange) {
    console.log("value tel sele: ",event.value);
  }
  isHombre():boolean {
    return this.asegurado.sexo == 1;
  }
}
