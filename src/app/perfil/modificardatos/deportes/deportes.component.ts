import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, NgModel } from '@angular/forms';
import { MatSelectChange } from '@angular/material';
import { TablasService } from '../../../_service/tablas.service';

@Component({
  selector: 'deportes',
  templateUrl: './deportes.component.html',
  styleUrls: ['./deportes.component.css'] 
})
export class DeportesComponent implements OnInit {
  public deportesForm = new FormControl();
  public deportes: any
  public deportesList:any;
  public checked:any;
  @Input() asegurado;
  @Output() valueChange = new EventEmitter();
  constructor(
    private _tablasService:TablasService
  ) {
    this.deportes = {}
   }

  ngOnInit() {
    this._tablasService.getTablas("w","deportes").subscribe(data=>{
      this.deportes = data.Tabla.Registros.Registro;
      this.checked = this.deportes.filter(deporte=>deporte.codigo == this.asegurado.deportes.deporte[0].codigo);
      for(let deporte of this.asegurado.deportes.deporte) {
        this.checked.push(this.deportes.filter(depor=>depor.codigo == this.padLeft(deporte.codigo,'0',3))[0]);
      }
      console.log("checked: ", this.checked);
      console.log(this.deportes);
    });
  }
  compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }  
   onSelectionChanged(event: MatSelectChange) {
    let values = event.value.map(deporte=>deporte);
    console.log(event);
    let deportes = [];    
    for(let value of values) {
      let deporte:any = {"codigo": value.codigo};
      deportes.push(deporte);
    }
    this.asegurado.deportes = deportes;

    this.valueChange.emit(this.asegurado.deportes);
    console.log("estado civil seleccionado: ",this.asegurado.deportes);
  }
  padLeft(text:string, padChar:string, size:number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }
}
