import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class ModificarDatosSservice {
    constructor
    (
        private _http: HttpClient,
    ) {

    }

    private extractData(res: Response) {
        let body = res;
        return body || { };
    }

    public obtenerDatosAsegurado(tipoDocumento, numeroDocumento):Observable<any> {
        let data = {
            "empresa": "A",
            "sucursal": "CA",
            "tipoDocumento": tipoDocumento,
            "numeroDocumento": numeroDocumento
        };
        return this._http.post(environment.urlService + "/autogestion/getDatosAsegurado", data).pipe(
            map(this.extractData)
        );
     }
     public actualizarDatos(asegurado, tipoDocumento, numeroDocumento) :Observable<any> {
        let data = { "asegurado": {
                "tipoDocumento": tipoDocumento,
                "numeroDocumento": numeroDocumento,
                "numeroAsegurado": asegurado.nroAsegurado,
                "codigoNacionalidad": asegurado.nacionalidad,
                "codigoSexo": asegurado.sexo,
                "numeroJubilacion": asegurado.cuil,
                "codigoEstadoCivil": asegurado.estadoCivil == undefined ? 0:asegurado.estadoCivil,
                "fechaNacimiento": asegurado.fechaNacimiento,
                "cantidadHijos": asegurado.cantidadDeHijos,
                "codigoNivelEstudios": asegurado.nivelDeEstudios,
                "fumador":asegurado.fumador?"S":"N",
                "codigoRamaActividadEconomica": asegurado.ramaActividadEc,
                "codigoProfesion":asegurado.profesion,
                "deportes":undefined,
                "emails": asegurado.emails,
                "telefonos": asegurado.telefonos
            }
        }
        if(asegurado.deportes.length > 0) {
            data.asegurado.deportes =  {
                "deporte": asegurado.deportes
            }
        }
        console.log("data", JSON.stringify(data));
        return this._http.post(environment.urlService + "/autogestion/actualizarDatosAsegurado", data).pipe(
            map(this.extractData)
        );
     }
     actualizarDatosFiliatorios(tipoDocumento, numeroDocumento, mensaje): Observable<any> {
         let data = {
             "empresa":"A",
             "sucursal:":"CA",
             "tipoDocumento":tipoDocumento,
             "numeroDocumento":numeroDocumento,
             "mensaje": mensaje
         }
         return this._http.post(environment.urlService + "/autogestion/actualizarDatosFiliatorios", data).pipe(
            map(this.extractData)
        );
     }
}