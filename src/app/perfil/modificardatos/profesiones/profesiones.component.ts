import { MatAutocompleteSelectedEvent } from '@angular/material';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TablasService } from '../../../_service/tablas.service';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'profesiones',
  templateUrl: './profesiones.component.html',
  styleUrls: ['./profesiones.component.css']
})
export class ProfesionesComponent implements OnInit {
  public profesionesControl = new FormControl();
  public filteredOptions: Observable<any[]>;
  public profesiones: any;
  public profesionSeleccionada: any;
  public options: string[];
  @Input() asegurado;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService,
  ) { 
    this.profesionSeleccionada = {};
    this.profesiones = {};
  }

  ngOnInit() {
    this._tablasService.getListaProfesiones().subscribe(data => {
      this.profesiones = data.return.Profesiones;
      console.log("data", data.return.Profesiones);
      this.profesionSeleccionada = this.profesiones.filter(profesion=>profesion.CodProf == this.asegurado.profesion)[0];
      if(this.profesionSeleccionada === undefined)
        this.profesionSeleccionada = {};
      console.log(this.profesionSeleccionada)
      this.filteredOptions = this.profesionesControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
      //console.log("profesiones",this.profesiones);
    });
  }

  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();
    return this.profesiones.filter(profesion => profesion.NomProf.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    let value = event.option.value.toLowerCase();
    this.profesionSeleccionada = this.profesiones.filter(profesion => profesion.NomProf.toLowerCase().indexOf(value) === 0)[0];
    this.asegurado.profesion = this.profesionSeleccionada.CodProf;
    this.valueChange.emit(this.asegurado);
    console.log("Profesion Seleccionada: ",this.asegurado.profesion);
  }
}