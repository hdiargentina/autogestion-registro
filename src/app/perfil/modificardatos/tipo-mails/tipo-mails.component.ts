import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material';
import { TablasService } from '../../../_service/tablas.service';

export interface mail {
  tipo:string;
  mail: string;
}

@Component({
  selector: 'tipo-mails',
  templateUrl: './tipo-mails.component.html',
  styleUrls: ['./tipo-mails.component.css']
})
export class TipoMailsComponent implements OnInit {
  public listaTipoMail:any;
  @Input() tipoMailSeleccionado;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService
  ) { 
    this.listaTipoMail = {};
  }

  ngOnInit() {
    this._tablasService.getListaTipoMail().subscribe(data=>{
      this.listaTipoMail = data.return.TiposDeMails;

      console.log("Lista tipo mail: ",this.listaTipoMail);
    });
  }
  onSelectionChanged(event: MatSelectChange) {
    let value = event.value.toLowerCase();
    console.log("value tel sele: ",event);
    let tipoMailSeleccionado = this.listaTipoMail.filter(mail=>mail.CodTipoCorreo.toLowerCase() == value)[0];
    this.tipoMailSeleccionado = tipoMailSeleccionado.CodTipoCorreo;
    this.valueChange.emit(event.value);
    console.log("tipo mail seleccionado: ",this.tipoMailSeleccionado);
  }
}
