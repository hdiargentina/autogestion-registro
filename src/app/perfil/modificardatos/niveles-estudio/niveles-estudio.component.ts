import { MatSelectChange } from '@angular/material';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TablasService } from '../../../_service/tablas.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'niveles-estudio',
  templateUrl: './niveles-estudio.component.html',
  styleUrls: ['./niveles-estudio.component.css'],
  providers: [TablasService]
})
export class NivelesEstudioComponent implements OnInit {
  public niveles_estudio:any;
  public nivel_estudio:any;
  @Input() asegurado;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService,
  ) {
    this.niveles_estudio = {};
    this.nivel_estudio = {};
   }

  ngOnInit() {
    this._tablasService.getTablas("t", "niveles_estudio").subscribe(data=> {
      this.niveles_estudio = data.Tabla.Registros.Registro;
      this.nivel_estudio = this.niveles_estudio.filter(nivelEstudio => nivelEstudio.codigo == this.asegurado.nivelDeEstudios)[0];
      this.nivel_estudio = this.nivel_estudio == undefined ? 0:this.nivel_estudio
      this.asegurado.nivelDeEstudios = this.nivel_estudio.codigo;
      console.log("niveles de estudio", this.niveles_estudio)
      console.log("nivel de estudio",this.asegurado.nivelDeEstudios);
    });
  }
  onSelectionChanged(event: MatSelectChange) {
    let value = event.value.codigo.toLowerCase();
    console.log(value);
    this.nivel_estudio = this.niveles_estudio.filter(nivel_estudio => nivel_estudio.codigo == value)[0];
    this.asegurado.nivelDeEstudios = this.nivel_estudio.codigo;
    this.valueChange.emit(this.asegurado);
    console.log("NivelEstudio Selecc: ",this.asegurado.nivelDeEstudios);
  }
}
