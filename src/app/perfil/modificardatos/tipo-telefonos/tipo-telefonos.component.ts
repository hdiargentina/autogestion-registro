import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material';
import { TablasService } from '../../../_service/tablas.service';

@Component({
  selector: 'tipo-telefonos',
  templateUrl: './tipo-telefonos.component.html',
  styleUrls: ['./tipo-telefonos.component.css']
})
export class TipoTelefonosComponent implements OnInit {
  public listaTiposTelefono: any;
  @Input() tipoTelefono;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService
  ) { 
    this.listaTiposTelefono = {};
  }

  ngOnInit() {
    this.listaTiposTelefono = this._tablasService.getListaTiposTelefono();
  }
  onSelectionChanged(event: MatSelectChange) {
    let value = event.value.toLowerCase();
    console.log("value tel sele: ",value);
    let tipoTelefonoSeleccionado = this.listaTiposTelefono.filter(telefono=>telefono.codigo.toLowerCase() == value)[0];
    this.tipoTelefono = tipoTelefonoSeleccionado.codigo;
    this.valueChange.emit(this.tipoTelefono);
    console.log("tipo telefono  seleccionado: ",this.tipoTelefono);
  }
}
