import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TablasService } from '../../../_service/tablas.service';

import { throttleTime } from 'rxjs/operators';
import { MatSelectChange } from '@angular/material';

@Component({
  selector: 'estado-civil',
  templateUrl: './estado-civil.component.html',
  styleUrls: ['./estado-civil.component.css']
})
export class EstadoCivilComponent implements OnInit {
  public listaEstadoCivil: any;
  public estadoCivil:any;
  @Input() asegurado;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _tablasService: TablasService
  ) { 
    this.estadoCivil = {};
  }

  ngOnInit() {
    this._tablasService.getListaEstadoCivil().subscribe(data=>{
        this.listaEstadoCivil = data.return.EstadoCivil;
        this.estadoCivil = this.listaEstadoCivil.filter(estadoCivil=>estadoCivil.CodEstCivil == this.asegurado.estadoCivil)[0];
        if(this.estadoCivil != undefined && this.estadoCivil != {})
          this.asegurado.estadoCivil = this.estadoCivil.CodEstCivil;
        else 
          this.asegurado.estadoCivil = 0;
         console.log("ListaEstadoCivil",this.listaEstadoCivil);
      });
  }
  onSelectionChanged(event: MatSelectChange) {
    let value = event.value.CodEstCivil.toLowerCase();
    console.log(value);
    this.estadoCivil = this.listaEstadoCivil.filter(estadoCivil=>estadoCivil.CodEstCivil == value)[0];
    this.asegurado.estadoCivil = this.estadoCivil.CodEstCivil;
    this.valueChange.emit(this.asegurado);
    console.log("estado civil seleccionado: ",this.asegurado.estadoCivil);
  }
}
