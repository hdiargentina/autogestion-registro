import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { SharedModule } from '../shared/shared.module';

import { ModificarDatosComponent } from './modificardatos/modificardatos.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { MAT_DATE_LOCALE } from '@angular/material';
import { NivelesEstudioComponent } from './modificardatos/niveles-estudio/niveles-estudio.component';
import { RamasActividadEconomicaComponent } from './modificardatos/ramas-actividad-economica/ramas-actividad-economica.component';
import { ProfesionesComponent } from './modificardatos/profesiones/profesiones.component';
import { NacionalidadesComponent } from './modificardatos/nacionalidades/nacionalidades.component';
import { TipoTelefonosComponent } from './modificardatos/tipo-telefonos/tipo-telefonos.component';
import { TipoMailsComponent } from './modificardatos/tipo-mails/tipo-mails.component';
import { EstadoCivilComponent } from './modificardatos/estado-civil/estado-civil.component';
import { DeportesComponent } from './modificardatos/deportes/deportes.component';

@NgModule({
    declarations: [
        ModificarDatosComponent,
        ConfiguracionComponent,
        NivelesEstudioComponent,
        DeportesComponent,
        RamasActividadEconomicaComponent,
        ProfesionesComponent,
        NacionalidadesComponent,
        TipoTelefonosComponent,
        TipoMailsComponent,
        EstadoCivilComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'es-AR'},
      ]
})
export class PerfilModule{ }